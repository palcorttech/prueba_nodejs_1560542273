import express, { json } from 'express';
import morgan from 'morgan';
//importar rutas

import usuarioRoutes from './routes/usuario.routes';
import alquilerRoutes from './routes/alquiler.routes';

//inicializacion
const app = express();

//middlewares
app.use(morgan('dev'));
app.use(json());

//routes

app.use('/api/usuario', usuarioRoutes);
app.use('/api/inventario', alquilerRoutes);

export default app;