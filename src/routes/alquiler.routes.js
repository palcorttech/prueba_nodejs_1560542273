import { Router } from 'express';
const router = Router();
import { disponiblePorTienda, Alquilar } from '../controllers/alquiler.controller';
// import { crearUsuario } from '../controllers/usuario.controller';

// crear usuario;
router.get('/disponibilidad', disponiblePorTienda);
router.post('/alquilar', Alquilar);

export default router;