import { Router } from 'express';
const router = Router();
import { crearUsuario } from '../controllers/usuario.controller';

// crear usuario;
router.post('/crear', crearUsuario);



export default router;