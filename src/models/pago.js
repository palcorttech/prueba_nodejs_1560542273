import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Pago = sequelize.define('payment', {
    payment_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    customer_id: {
        type: Sequelize.INTEGER
    },
    staff_id: {
        type: Sequelize.INTEGER
    },
    rental_id: {
        type: Sequelize.INTEGER
    },
    amount: {
        type: Sequelize.INTEGER
    },
    payment_date: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


export default Pago;