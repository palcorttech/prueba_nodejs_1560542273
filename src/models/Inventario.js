import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Inventario = sequelize.define('inventory', {
    inventory_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    film_id: {
        type: Sequelize.INTEGER
    },
    store_id: {
        type: Sequelize.INTEGER
    },
    last_update: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


export default Inventario;