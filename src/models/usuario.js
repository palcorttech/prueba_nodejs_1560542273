import Sequelize from 'sequelize';
import { sequelize } from '../database/database';


import Address from './Address';

const Usuario = sequelize.define('customer', {
    customer_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    store_id: {
        type: Sequelize.INTEGER
    },
    first_name: {
        type: Sequelize.TEXT
    },
    last_name: {
        type: Sequelize.TEXT
    },
    email: {
        type: Sequelize.TEXT
    },
    address_id: {
        type: Sequelize.INTEGER
    },
    activebool: {
        type: Sequelize.BOOLEAN
    },
    create_date: {
        type: Sequelize.DATE
    },
    last_update: {
        type: Sequelize.DATE
    },
    active: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


export default Usuario;