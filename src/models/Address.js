import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Usuario from './usuario';

const Address = sequelize.define('address', {
    address_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },

    address: {
        type: Sequelize.TEXT
    },
    address2: {
        type: Sequelize.TEXT
    },
    district: {
        type: Sequelize.TEXT
    },
    city_id: {
        type: Sequelize.INTEGER
    },
    postal_code: {
        type: Sequelize.TEXT
    },
    phone: {
        type: Sequelize.INTEGER
    },
    last_update: {
        type: Sequelize.DATE
    },
    active: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});

// Project.hasMany(Tasks, { foreignKey: 'projectid', sourceKey: 'id' });
// Tasks.belongsTo(Project, { foreignKey: 'projectid', sourceKey: 'id' });


export default Address;