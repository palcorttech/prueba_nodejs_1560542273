import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Renta = sequelize.define('rental', {
    rental_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    rental_date: {
        type: Sequelize.DATE
    },
    inventory_id: {
        type: Sequelize.INTEGER
    },
    customer_id: {
        type: Sequelize.INTEGER
    },
    return_date: {
        type: Sequelize.DATE
    },
    staff_id: {
        type: Sequelize.INTEGER
    },
    last_update: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


export default Renta;