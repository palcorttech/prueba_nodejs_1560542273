import Inventario from '../models/Inventario';
import Renta from '../models/renta';
import Pago from '../models/pago';


// verificar existencia de una pelicula por tienda

export async function disponiblePorTienda(req, res) {

    const { film_id, store_id } = req.body;
    try {
        const unidades = await Inventario.findAll({
            where: {
                film_id,
                store_id
            }
        });
        res.json({
            message: `existen ${unidades.length} unidades de la pelicula en la tienda consultada`,
            unidades: unidades.length,
            data: unidades
        })
    } catch (error) {
        console.log(error);
    }
}

// realizar alquiler
export async function Alquilar(req, res) {
    console.log(req.body);
    const { rental_date, inventory_id, return_date, staff_id, customer_id } = req.body.renta;

    const { amount, payment_date } = req.body.pago;

    try {
        let alquiler = await Renta.create({
            rental_date,
            inventory_id,
            return_date,
            staff_id,
            customer_id,

        }, {
            fields: ['rental_date', 'inventory_id', 'return_date', 'staff_id', 'customer_id']
        })

        if (alquiler) {
            let pago = await Pago.create({
                customer_id,
                staff_id,
                rental_id: alquiler.rental_id,
                amount,
                payment_date
            }, {
                fields: ['customer_id', 'staff_id', 'rental_id', 'amount', 'payment_date']
            })
            if (pago) {
                res.json({ message: 'se realizo correctamente el alquiler y el pago', alquiler, pago })
            }
        }

    } catch (error) {
        console.log('ocurrio e error:', error);
        res.json({ message: 'ocurrio un error en el proceso de alquilar y pagar', error })
    }


}