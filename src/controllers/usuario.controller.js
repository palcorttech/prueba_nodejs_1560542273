import Usuario from '../models/usuario';

// crear el cliente 
export async function crearUsuario(req, res) {

    const { store_id, first_name, last_name, email, address_id, activebool, create_date, last_update } = req.body;

    console.log('llego esto:', req.body);

    try {

        const email_existe = await Usuario.findOne({
            attributes: ['email'],
            where: {

                email
            }
        });
        if (email_existe) {
            res.json({ message: 'este usuario ya existe en el sistema' })
        } else {


            let newUser = await Usuario.create({
                store_id,
                first_name,
                last_name,
                email,
                address_id,
                activebool,
                create_date,


            }, {
                fields: ['store_id', 'first_name', 'last_name', 'email', 'address_id', 'activebool', 'create_date']
            });

            if (newUser) {

                res.json({
                    message: 'se creo el usuario',
                    data: newUser
                });

            }
        }


    } catch (error) {
        console.log('error crear usuario:', error);
    }
}