"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.disponiblePorTienda = disponiblePorTienda;
exports.Alquilar = Alquilar;

var _Inventario = _interopRequireDefault(require("../models/Inventario"));

var _renta = _interopRequireDefault(require("../models/renta"));

var _pago = _interopRequireDefault(require("../models/pago"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// verificar existencia de una pelicula por tienda
function disponiblePorTienda(_x, _x2) {
  return _disponiblePorTienda.apply(this, arguments);
} // realizar alquiler


function _disponiblePorTienda() {
  _disponiblePorTienda = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var _req$body, film_id, store_id, unidades;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, film_id = _req$body.film_id, store_id = _req$body.store_id;
            _context.prev = 1;
            _context.next = 4;
            return _Inventario["default"].findAll({
              where: {
                film_id: film_id,
                store_id: store_id
              }
            });

          case 4:
            unidades = _context.sent;
            res.json({
              message: "existen ".concat(unidades.length, " unidades de la pelicula en la tienda consultada"),
              unidades: unidades.length,
              data: unidades
            });
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            console.log(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 8]]);
  }));
  return _disponiblePorTienda.apply(this, arguments);
}

function Alquilar(_x3, _x4) {
  return _Alquilar.apply(this, arguments);
}

function _Alquilar() {
  _Alquilar = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body$renta, rental_date, inventory_id, return_date, staff_id, customer_id, _req$body$pago, amount, payment_date, alquiler, pago;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            console.log(req.body);
            _req$body$renta = req.body.renta, rental_date = _req$body$renta.rental_date, inventory_id = _req$body$renta.inventory_id, return_date = _req$body$renta.return_date, staff_id = _req$body$renta.staff_id, customer_id = _req$body$renta.customer_id;
            _req$body$pago = req.body.pago, amount = _req$body$pago.amount, payment_date = _req$body$pago.payment_date;
            _context2.prev = 3;
            _context2.next = 6;
            return _renta["default"].create({
              rental_date: rental_date,
              inventory_id: inventory_id,
              return_date: return_date,
              staff_id: staff_id,
              customer_id: customer_id
            }, {
              fields: ['rental_date', 'inventory_id', 'return_date', 'staff_id', 'customer_id']
            });

          case 6:
            alquiler = _context2.sent;

            if (!alquiler) {
              _context2.next = 12;
              break;
            }

            _context2.next = 10;
            return _pago["default"].create({
              customer_id: customer_id,
              staff_id: staff_id,
              rental_id: alquiler.rental_id,
              amount: amount,
              payment_date: payment_date
            }, {
              fields: ['customer_id', 'staff_id', 'rental_id', 'amount', 'payment_date']
            });

          case 10:
            pago = _context2.sent;

            if (pago) {
              res.json({
                message: 'se realizo correctamente el alquiler y el pago',
                alquiler: alquiler,
                pago: pago
              });
            }

          case 12:
            _context2.next = 18;
            break;

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](3);
            console.log('ocurrio e error:', _context2.t0);
            res.json({
              message: 'ocurrio un error en el proceso de alquilar y pagar',
              error: _context2.t0
            });

          case 18:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[3, 14]]);
  }));
  return _Alquilar.apply(this, arguments);
}