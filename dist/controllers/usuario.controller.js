"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.crearUsuario = crearUsuario;

var _usuario = _interopRequireDefault(require("../models/usuario"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// crear el cliente 
function crearUsuario(_x, _x2) {
  return _crearUsuario.apply(this, arguments);
}

function _crearUsuario() {
  _crearUsuario = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var _req$body, store_id, first_name, last_name, email, address_id, activebool, create_date, last_update, email_existe, newUser;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, store_id = _req$body.store_id, first_name = _req$body.first_name, last_name = _req$body.last_name, email = _req$body.email, address_id = _req$body.address_id, activebool = _req$body.activebool, create_date = _req$body.create_date, last_update = _req$body.last_update;
            console.log('llego esto:', req.body);
            _context.prev = 2;
            _context.next = 5;
            return _usuario["default"].findOne({
              attributes: ['email'],
              where: {
                email: email
              }
            });

          case 5:
            email_existe = _context.sent;

            if (!email_existe) {
              _context.next = 10;
              break;
            }

            res.json({
              message: 'este usuario ya existe en el sistema'
            });
            _context.next = 14;
            break;

          case 10:
            _context.next = 12;
            return _usuario["default"].create({
              store_id: store_id,
              first_name: first_name,
              last_name: last_name,
              email: email,
              address_id: address_id,
              activebool: activebool,
              create_date: create_date
            }, {
              fields: ['store_id', 'first_name', 'last_name', 'email', 'address_id', 'activebool', 'create_date']
            });

          case 12:
            newUser = _context.sent;

            if (newUser) {
              res.json({
                message: 'se creo el usuario',
                data: newUser
              });
            }

          case 14:
            _context.next = 19;
            break;

          case 16:
            _context.prev = 16;
            _context.t0 = _context["catch"](2);
            console.log('error crear usuario:', _context.t0);

          case 19:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[2, 16]]);
  }));
  return _crearUsuario.apply(this, arguments);
}