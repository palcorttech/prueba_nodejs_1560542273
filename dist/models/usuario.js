"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _Address = _interopRequireDefault(require("./Address"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Usuario = _database.sequelize.define('customer', {
  customer_id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  store_id: {
    type: _sequelize["default"].INTEGER
  },
  first_name: {
    type: _sequelize["default"].TEXT
  },
  last_name: {
    type: _sequelize["default"].TEXT
  },
  email: {
    type: _sequelize["default"].TEXT
  },
  address_id: {
    type: _sequelize["default"].INTEGER
  },
  activebool: {
    type: _sequelize["default"].BOOLEAN
  },
  create_date: {
    type: _sequelize["default"].DATE
  },
  last_update: {
    type: _sequelize["default"].DATE
  },
  active: {
    type: _sequelize["default"].INTEGER
  }
}, {
  timestamps: false,
  freezeTableName: true
}); // Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


var _default = Usuario;
exports["default"] = _default;