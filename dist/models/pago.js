"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Pago = _database.sequelize.define('payment', {
  payment_id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  customer_id: {
    type: _sequelize["default"].INTEGER
  },
  staff_id: {
    type: _sequelize["default"].INTEGER
  },
  rental_id: {
    type: _sequelize["default"].INTEGER
  },
  amount: {
    type: _sequelize["default"].INTEGER
  },
  payment_date: {
    type: _sequelize["default"].DATE
  }
}, {
  timestamps: false,
  freezeTableName: true
}); // Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


var _default = Pago;
exports["default"] = _default;