"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _usuario = _interopRequireDefault(require("./usuario"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Address = _database.sequelize.define('address', {
  address_id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  address: {
    type: _sequelize["default"].TEXT
  },
  address2: {
    type: _sequelize["default"].TEXT
  },
  district: {
    type: _sequelize["default"].TEXT
  },
  city_id: {
    type: _sequelize["default"].INTEGER
  },
  postal_code: {
    type: _sequelize["default"].TEXT
  },
  phone: {
    type: _sequelize["default"].INTEGER
  },
  last_update: {
    type: _sequelize["default"].DATE
  },
  active: {
    type: _sequelize["default"].INTEGER
  }
}, {
  timestamps: false
}); // Project.hasMany(Tasks, { foreignKey: 'projectid', sourceKey: 'id' });
// Tasks.belongsTo(Project, { foreignKey: 'projectid', sourceKey: 'id' });


var _default = Address;
exports["default"] = _default;