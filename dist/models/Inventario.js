"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Inventario = _database.sequelize.define('inventory', {
  inventory_id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  film_id: {
    type: _sequelize["default"].INTEGER
  },
  store_id: {
    type: _sequelize["default"].INTEGER
  },
  last_update: {
    type: _sequelize["default"].DATE
  }
}, {
  timestamps: false,
  freezeTableName: true
}); // Usuario.hasOne(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });
// Usuario.belongsTo(Address, { foreignKey: 'address_id', sourceKey: 'address_id' });


var _default = Inventario;
exports["default"] = _default;