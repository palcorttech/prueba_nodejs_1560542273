"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _usuario = require("../controllers/usuario.controller");

var router = (0, _express.Router)();
// crear usuario;
router.post('/crear', _usuario.crearUsuario);
var _default = router;
exports["default"] = _default;