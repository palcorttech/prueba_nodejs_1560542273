"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _alquiler = require("../controllers/alquiler.controller");

var router = (0, _express.Router)();
// import { crearUsuario } from '../controllers/usuario.controller';
// crear usuario;
router.get('/disponibilidad', _alquiler.disponiblePorTienda);
router.post('/alquilar', _alquiler.Alquilar);
var _default = router;
exports["default"] = _default;